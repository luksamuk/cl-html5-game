;;;; package.lisp

(defpackage #:html5-game
  (:use #:cl #:cl-who #:parenscript)
  (:export #:get-game-as-string))

