;;;; html5-game.asd

(asdf:defsystem #:html5-game
  :description "A game written in Common Lisp which translates to HTML5"
  :author "Lucas Vieira <lucasvieira@lisp.com.br>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:parenscript
	       #:cl-who
	       #:varjo)
  :components ((:file "package")
               (:file "html5-game")))

(asdf:defsystem #:html5-game/test
  :description "Test suite for html5-game"
  :author "Lucas Vieira <lucasvieira@lisp.com.br>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:html5-game
	       #:hunchentoot)
  :components ((:file "package-test")
               (:file "html5-game-test")))
