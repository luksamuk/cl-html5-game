;;;; html5-game-test.lisp

(in-package #:html5-game-test)

(defparameter *acceptor* nil)

(defun start-test-server ()
  ;; Start Hunchentoot server
  (stop-test-server)
  (start (setf *acceptor*
	       (make-instance 'easy-acceptor :port 3000)))
  (define-easy-handler (game-handler :uri "/") ()
    (get-game-as-string)))

(defun server-running-p ()
  (and (not (null *acceptor*))
       (started-p *acceptor*)))

(defun stop-test-server ()
  (when (server-running-p)
      (hunchentoot:stop *acceptor*)))
