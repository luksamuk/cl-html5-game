;;;; html5-game.lisp

(in-package #:html5-game)

;; Setup for making cl-who and Parenscript work together
(setf *js-string-delimiter* #\")
(setq cl-who:*attribute-quote-char* #\")

(defparameter *shader*
  (varjo:glsl-code
   (varjo:translate
    (varjo:make-stage :fragment nil
		      '((a-position :vec2 :attribute))
		      '(:330)
		      '((vari:vec4 0 1 0 1))))))

(defparameter *game-macros*
  (ps
    ;; grrrrr i needed those
    (defmacro car (list)
      `(@ ,list 0))
    (defmacro cadr (list)
      `(@ ,list 1))

    (defmacro princ (&rest rest)
      `((@ console log)
	,@rest))

    (defun deg-to-rad (angle)
      (/ (* angle pi) 180))

    (defmacro request-animation-frame (callback)
      `((@ window request-animation-frame) ,callback))

    ;; Drawing macros
    (defmacro clear (context size)
      `((@ ,context clear-rect) 0 0
	(car ,size) (cadr ,size)))
    
    (defmacro begin-path (context)
      `((@ ,context begin-path)))

    (defmacro close-path (context)
      `((@ ,context close-path)))

    (defmacro move-to (context vertex)
      `((@ ,context move-to) (car ,vertex) (cadr ,vertex)))
    
    (defmacro line-to (context vertex)
      `((@ ,context line-to) (car ,vertex) (cadr ,vertex)))

    (defmacro rect (context position size)
      `((@ ,context rect)
	(car ,position) (cadr ,position)
	(car ,size) (cadr ,size)))

    (defmacro arc (context position radius begin-angle end-angle)
      `((@ ,context arc)
	(car ,position) (cadr ,position)
	,radius
	,begin-angle ,end-angle))

    (defmacro fill (context color)
      `(progn
	 (setf (@ ,context fill-style) ,color)
	 ((@ ,context fill))))

    ;; Event listeners
    (defmacro add-event-listener (event callback)
      `((@ document add-event-listener)
	(ps:stringify ,event)
	,callback
	nil)))
  "Those are macros I might need for development. Anythime I needed a macro,
I dropped it here.")

(defmacro on-game-page (&body body)
  "Evaluates the body as a string to be contained as script.
Use Parenscript for scripting."
  `(with-html-output-to-string (s)
     (:html :lang "en"
	    (:head
	     (:meta :charset "UTF-8")
	     (:title "Parenscript HTML5 Game")
	     (:script :type "text/javascript" (str *game-macros*)))
	    (:body
	     (:canvas :id "viewport" :width "800" :height "600")
	     (:script :type "text/javascript"
		      (str ,@body))))))


(defun get-game-as-string ()
  (on-game-page
    (ps
      (defvar canvas ((@ document get-element-by-id) "viewport"))
      (defvar context ((@ canvas get-context) "2d"))
      ;;(defvar frame-time (/ 1000 60))
      (defvar play-area (list (@ canvas width) (@ canvas height)))
      
      (setf (@ context image-smoothing-enabled) t)

      (defvar +black+ "#000")
      (defvar +white+ "#fff")

      (defvar *angle* 0)

      
      ;; Event listeners
      (add-event-listener
       'resize
       (lambda ()
	 (setf play-area
	       (list (@ canvas width) (@ canvas height)))))

      (add-event-listener
       'mousedown
       (lambda (event)
	 (princ "Button: " (@ event which))))
				

      ;; Related to game loop
      (defun update ()
	(setf *angle*
	      (rem (+ 5 *angle*) 360)))
      
      (defun draw ()
	(clear context play-area)
	(begin-path context)
	(rect context '(100 100) '(100 100))
	(fill context +black+)
	(close-path context)

	(begin-path context)
	(arc context '(150 150) 20 0 (* pi 2))
	(fill context +white+)
	(close-path context)

	(let ((position (list (+ 150
				 (* (cos (deg-to-rad *angle*))
				    50))
			      (+ 150
				 (* (sin (deg-to-rad *angle*))
				    50)))))
	  (begin-path context)
	  (arc context position 20 0 (* pi 2))
	  (fill context "#aaffee")
	  (close-path context)))
      
      (defun game-loop ()
	(update)
	(draw)
	(request-animation-frame #'game-loop))
      

      ;;(set-interval #'game-loop frame-time)
      (request-animation-frame #'game-loop))))
