# html5-game

This is an attempt at creating an HTML5 game by using Common Lisp (and Parenscript)

## License

This project uses the MIT License. See [LICENSE](LICENSE) or [html5-game.asd](html5-game.asd) for details.
