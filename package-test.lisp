;;;; package-test.lisp

(defpackage #:html5-game-test
  (:use #:cl #:html5-game #:hunchentoot)
  (:export #:start-test-server
	   #:server-running-p
	   #:stop-test-server))


